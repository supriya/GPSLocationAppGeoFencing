package app.com.example.android.locationsample.view;

import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;

import app.com.example.android.locationsample.utils.PermissionUtil;
import app.com.example.android.locationsample.R;
import app.com.example.android.locationsample.services.LocationUpdateService;

public class MainActivity extends AppCompatActivity {

    protected static final String TAG = "MainActivity";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;



    /**
     * Used to keep track of whether geofences were added.
     */
    private boolean mGeofencesAdded;

    /**
     * Used when requesting to add or remove geofences.
     */
    private PendingIntent mGeofencePendingIntent;

    /**
     * Used to persist application state about whether geofences were added.
     */
    private SharedPreferences mSharedPreferences;

    // Buttons for kicking off the process of adding or removing geofences.
    private Button mAddGeofencesButton;
    private Button mRemoveGeofencesButton;


    Button mStartUpdatesButton;
    private boolean mIsServiceStarted;
    private Button mStopUpdatesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStartUpdatesButton = (Button) findViewById(R.id.loc_btn_start);
        mStartUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(PermissionUtil.isVersionMarshmallowAndAbove()){
                    //check for permission
                    if(!PermissionUtil.checkAccessLocationPermission(MainActivity.this)){
                        PermissionUtil.requestAccessLocationPermission(MainActivity.this);
                    }else{
                        startUpdatesButtonHandler();
                    }

                }else {
                    startUpdatesButtonHandler();
                }
            }
        });

        mStopUpdatesButton = (Button) findViewById(R.id.loc_btn_stop);
        mStopUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopUpdatesButtonHandler();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionUtil.REQUEST_CODE_ACCESS_LOCATION) {
            if (PermissionUtil.verifyPermissions(grantResults)) {
                //permission is given
                startUpdatesButtonHandler();
            } else {
                int rationalState = PermissionUtil.shouldShowRequestPermissionRationaleState(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION, this);
                if (rationalState == PermissionUtil.REQUEST_PERMISSION_SHOW_RATIONALE) {
                } else {
                    showAccessLocationAlertDialog();
                }
            }
        }
    }

    /**
     *  If location services not enabled show alert to enable it in marshmallow
     */
    private void showAccessLocationAlertDialog() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("open setting");
            builder.setPositiveButton("settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    PermissionUtil.openAppSettingPage(MainActivity.this);
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing
                }
            });
            builder.show();
        } catch (Exception e) {
        }
    }

    /**
     * Handles the Start Updates button and requests start of location updates. Does nothing if
     * updates have already been requested.
     */
    public void startUpdatesButtonHandler() {

        if(!PermissionUtil.isRelayLocationServiceRunning(this)) {
            setButtonsEnabledState();
            startService(new Intent(this, LocationUpdateService.class));
        }

    }

    /**
     * Ensures that only one button is enabled at any time. The Start Updates button is enabled
     * if the user is not requesting location updates. The Stop Updates button is enabled if the
     * user is requesting location updates.
     */
    private void setButtonsEnabledState() {
        if (PermissionUtil.isRelayLocationServiceRunning(this)) {
            mStartUpdatesButton.setEnabled(false);
            mStopUpdatesButton.setEnabled(true);
        } else {
            mStartUpdatesButton.setEnabled(true);
            mStopUpdatesButton.setEnabled(false);
        }
    }

    /**
     * Handles the Stop Updates button, and requests removal of location updates. Does nothing if
     * updates were not previously requested.
     */
    public void stopUpdatesButtonHandler() {
        if (PermissionUtil.isRelayLocationServiceRunning(this)) {
            setButtonsEnabledState();
            stopService(new Intent(this, LocationUpdateService.class));
        }
    }





}
